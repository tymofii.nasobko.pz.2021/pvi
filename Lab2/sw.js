var CACHE_NAME = 'my-site-cache-v1';
var urlsToCache = ['style.css','script.js','index.html'];

self.addEventListener('install',event=>{
    event.waitUntil(caches.open(CACHE_NAME).then(cache=>{
        return cache.addAll(urlsToCache);
    })
    );
})