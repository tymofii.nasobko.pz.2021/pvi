<?php
$server = "127.0.0.1";
$login = "root";
$pass = "";
$name_db = "students";


if ($_SERVER["REQUEST_METHOD"] == "GET") {
    $link = mysqli_connect($server, $login, $pass, $name_db);
    if (!$link) {
        echo json_encode("Server is not connected");
    }
    else {
        $result = mysqli_query($link, "SELECT * FROM `students-info`");
        $students = mysqli_fetch_all($result);
        echo json_encode($students);
    }
}


if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $group = htmlspecialchars(trim($_POST['group']));
    $name = htmlspecialchars(trim($_POST['first_name']));
    $surname = htmlspecialchars(trim($_POST['last_name']));
    $gender = htmlspecialchars(trim($_POST['gender']));
    $date = htmlspecialchars(trim($_POST['trip-start']));
    $type = $_POST['type'];

    $link = mysqli_connect($server, $login, $pass, $name_db);

    if (!$link) {
        echo json_encode("Server is not connected");

    }
    else {
        if($type=="update"){
            $id = ($_POST['myid']);
            $response = array('group' => $group, 'name' => $name, 'surname' => $surname, 'gender' => $gender, 'date' => $date);
            mysqli_query($link, "UPDATE `students`.`students-info` SET `Group` = '$group',`FirstName` = '$name',`LastName` = '$surname',`Gender` = '$gender',`Birthday` = '$date' WHERE `students-info`.`id` ='$id';");
            echo json_encode($response);
        }
        else if($type=="add"){
            $response = array('group' => $group, 'name' => $name, 'surname' => $surname, 'gender' => $gender, 'date' => $date);
            $link = mysqli_connect($server, $login, $pass, $name_db);
            mysqli_query($link, "INSERT INTO `students`.`students-info` (`id` ,`Group` ,`FirstName` ,`LastName`,`Gender` ,`Birthday`) VALUES (NULL, '$group', '$name','$surname', '$gender','$date')");
            echo json_encode($response);
        }
        else {
            $id = ($_POST['myid']);
            $result = mysqli_query($link, "DELETE FROM `students`.`students-info` WHERE `students-info`.`id` ='$id';");
            echo json_encode($result);
        }
    }
}

?>