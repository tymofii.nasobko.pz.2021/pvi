const student_action_Dialog = document.getElementById("panel-add");
const student_warning_Dialog= document.getElementById("panel-warning");
const student_Dialog = document.getElementsByClassName("dialog");


const closeDialog = document.getElementById("close-dialog");
const addStudent = document.getElementById("ok-dialog");
const editStudent = document.getElementById("edit-dialog");
const deleteStudent = document.getElementById("ok-warning-dialog");
const cancelWarning = document.getElementById("cancel-warning-dialog");
const closeWarningDialog = document.getElementById("close-warning-dialog");
const table_students = document.getElementById("the_table");

const group_input = document.getElementById("new_group")
const name_input = document.getElementById("new_name");
const surname_input = document.getElementById("new_surname");
const gender_input = document.getElementById("new_gender");
const birthday_input = document.getElementById("new_birthday");
const select_all_table = document.getElementById("select_all_table");
const hidden = document.getElementById("userId");

const form = document.getElementById('student-info');

var Students_List = [];

class Student {

    constructor(group,name,surname, gender,birthday) {
        this.group=group;
        this.name = name;
        this.surname = surname;
        this.gender = gender;
        this.birthday = birthday;
        this.id=0;
    }
    fill_student(){
        this.group = group_input.options[group_input.selectedIndex].text;
        this.name=name_input.value;
        this.surname = surname_input.value;
        this.gender = gender_input.options[gender_input.selectedIndex].text;
        this.birthday= birthday_input.value;
    }

    write_to_json(){
        const studentJSON = JSON.stringify(this);
        console.log(studentJSON);
    }


    refresh_student(){
        table_students.rows[this.student_number-1].cells[1].textContent = this.group;
        table_students.rows[this.student_number-1].cells[2].textContent = this.name;
        table_students.rows[this.student_number-1].cells[3].textContent = this.gender;
        table_students.rows[this.student_number-1].cells[4].textContent = this.birthday;


    }
    add_student() {
        const tbody = table_students.getElementsByTagName("tbody")[0];    // get reference to the table element and the tbody element
        const newRow = document.createElement("tr");    // create a new row
        newRow.className="student"+tbody.rows.length;
        // create new cells and set content
        const newCheckbox = document.createElement("input");
        newCheckbox.type = "checkbox";
        const new_Check = document.createElement("td");
        new_Check.appendChild(newCheckbox);

        const new_Group = document.createElement("td");
        new_Group.textContent = this.group;

        const new_Name = document.createElement("td");
        new_Name.textContent = this.name + " " + this.surname;

        const new_Gender = document.createElement("td");
        new_Gender.textContent = this.gender;


        const new_Birthday = document.createElement("td");
        new_Birthday.textContent = this.birthday;

        const new_Status = document.createElement("td");
        const newStatus = document.createElement("i");
        newStatus.className="fa fa-circle";
        newStatus.style.color="green";
        new_Status.appendChild(newStatus);

        const new_Options = document.createElement("td");
        const newOptions1 = document.createElement("i");
        const newOptions2 = document.createElement("i");

        newOptions1.className="fa fa-edit";
        newOptions1.style="font-size:24px";

        newOptions2.className="fa fa-close";
        newOptions2.style="font-size:24px";

        new_Options.appendChild(newOptions1);
        new_Options.appendChild(newOptions2);

        // append the cells to the row
        newRow.appendChild(new_Check);
        newRow.appendChild(new_Group);
        newRow.appendChild(new_Name);
        newRow.appendChild(new_Gender);
        newRow.appendChild(new_Birthday);
        newRow.appendChild(new_Status);
        newRow.appendChild(new_Options);

        // append the row to the tbody
        tbody.appendChild(newRow);

        this.write_to_json();

        newOptions1.name = this.id;
        newOptions2.name = this.id;


        newOptions1.addEventListener("click", function (){
            let index = this.parentNode.parentNode.rowIndex; // HERE IS INDEX OF THE CLICKED ROW
            const description = document.getElementById("dialog-description");
            const rows = table_students.getElementsByTagName("tr");
            const cells = rows[index].getElementsByTagName("td");
            group_input.value = cells[1].textContent;
            name_input.value= cells[2].textContent.split(" ")[0];
            surname_input.value= cells[2].textContent.split(" ")[1];
            gender_input.value = cells[3].textContent;
            birthday_input.value = cells[4].textContent;
            hidden.value = this.name;
            description.textContent = 'Edit student';
            student_action_Dialog.style.visibility = "visible";
            addStudent.style.display ="none";
            editStudent.style.display ="inline";
            editStudent.name = index;
        });
        newOptions2.addEventListener("click", function (){
            var index = this.parentNode.parentNode.rowIndex; // HERE IS INDEX OF THE CLICKED ROW
            deleteStudent.name=index;
            student_warning_Dialog.style.visibility = "visible";
            var warning_text = document.getElementById("warning-text");
            warning_text.textContent = "Are you sure you want to delete";
            var checkboxes = document.querySelectorAll('#the_table input[type="checkbox"]');
            var checked = 0;
            var checked_index = -1;
            for (var i = 1; i < checkboxes.length; i++) {
                if(checkboxes[i].checked == true){
                    checked++;
                    if(checked_index==-1) {
                        checked_index = i;
                    }
                }
            }
            if((checked==1 || checked ==0) && (checked_index==index || checked_index==-1)){
                //Getting a name
                const rows = table_students.getElementsByTagName("tr");
                const cells = rows[index].getElementsByTagName("td");
                var name = cells[2].textContent;
                warning_text.textContent = warning_text.textContent +" " + name + "?";
                //Getting a name
            }
            else{
                if(checkboxes[index].checked==false){
                    checked= checked+1;
                }
                warning_text.textContent = warning_text.textContent +" " + checked + " elements?";
            }
        });
    }
}

function submit_student(){
    if(!allLetter(name_input)){
        return;
    }
    if(!allLetter(surname_input)){
        return;
    }
    if(add_server()){
        location.reload();
    }
    else{
        alert("Couldn't add the student into the database");
    }
    student_action_Dialog.style.visibility = "hidden";

}


addStudent.addEventListener("click", submit_student);

// get reference to the table element and the tbody element
document.getElementById("add_student").addEventListener("click", function() {

    group_input.value = "";
    name_input.value = "";
    surname_input.value = "";
    gender_input.value = "";
    birthday_input.value = "";

    student_action_Dialog.style.visibility = "visible";
    const description = document.getElementById("dialog-description");
    description.textContent = 'Add student';
    addStudent.style.display ="inline";
    editStudent.style.display ="none";


});
// Hide the dialog when the close button is clicked
closeDialog.addEventListener("click", function() {
    student_action_Dialog.style.visibility = "hidden";
    addStudent.style.display ="inline";
    editStudent.style.display ="inline";

});

editStudent.addEventListener("click", function(){

    const data = new FormData(form);
    data.append("myid",data.get("user"));
    data.append("type","update");
    for (const [key, value] of data.entries()) {
        console.log(key, value);
    }
    fetch('/action.php', {
        method: 'POST',
        body: data
    })
        .then(response => response.json())
        .then(data => {
            console.log(data);
            const table_students = document.getElementById("the_table");
            const rows = table_students.getElementsByTagName("tr");
            const cells = rows[parseInt(this.name)].getElementsByTagName("td");
            cells[1].textContent = data["group"];
            cells[2].textContent = data["name"]+ " " + data["surname"];
            cells[3].textContent = data["gender"];
            cells[4].textContent = data["date"];

            student_action_Dialog.style.visibility = "hidden";
        })
        .catch(error => {
            alert("Couldn't update the student in the database")
            console.error(error);

        });
});

deleteStudent.addEventListener("click",function(){
    console.log(Students_List);
    let checked = false;
    let ToDelete = [];
    let deleted = false;

    student_warning_Dialog.style.visibility = "hidden";
    var checkboxes = document.querySelectorAll('#the_table input[type="checkbox"]');
    for (var i = checkboxes.length-1; i >=1; i--) {
        if(checkboxes[i].checked == true){
            checked = true;
            ToDelete.push(i);
            let student = Students_List[i-1];
            const data = new FormData(form);
            data.append("myid",student.id);
            fetch('/action.php', {
                method: 'POST',
                body: data
            })
                .then(response => response.json())
                .then(data => {
                    console.log(data);
                    if(data==true && deleted == false){
                        for(var i = 0; i < ToDelete.length; i++){
                            Students_List.splice(ToDelete[i], 1);
                            table_students.deleteRow(ToDelete[i]);
                        }
                        deleted = true;

                    }
                })
                .catch(error => {
                    console.error(error);
                });
        }

    }
    if(checked==false){
        var error = false;
        let index = this.name;
        let data_delete;
        let student = new Student();
        student = Students_List[index-1];
        const data = new FormData(form);
        data.append("myid",student.id);
        fetch('/action.php', {
            method: 'POST',
            body: data
        })
            .then(response => response.json())
            .then(data => {
                console.log(data);
                Students_List.splice(index, 1);
                table_students.deleteRow(index);
                deleted=true;

            })
            .catch(error => {
                error=true;
                console.error(error);
            });
    }
    if(error==false){
        location.reload();
    }
    else{
        alert("Couldn't delete the student from the database")

    }

});
cancelWarning.addEventListener("click", function() {
    student_warning_Dialog.style.visibility = "hidden";
    addStudent.style.display = "inline";
});

closeWarningDialog.addEventListener("click", function() {
    student_warning_Dialog.style.visibility = "hidden";
    addStudent.style.display = "inline";
});

document.getElementById("notification").addEventListener("click", function() {
    const notificate = document.querySelector("#notified");
    if(notificate.style.visibility == "visible"){
        notificate.style.visibility = "hidden";
    }
    else{
        notificate.style.visibility = "visible";
    }

    notificate.classList.toggle("pulse");
});

const dashboard = document.getElementById("dashboard");
const students = document.getElementById("students");
const tasks = document.getElementById("tasks");

dashboard.addEventListener("click", function() {
    if (dashboard.style.fontWeight == "bold") {
        dashboard.style.fontWeight = "normal";
    } else {
        dashboard.style.fontWeight = "bold"
        tasks.style.fontWeight = "normal";
        students.style.fontWeight = "normal";
    }
});
students.addEventListener("click", function() {
    if (students.style.fontWeight == "bold") {
        students.style.fontWeight = "normal";
    } else {
        students.style.fontWeight = "bold"
        tasks.style.fontWeight = "normal";
        dashboard.style.fontWeight = "normal";
    }
});
tasks.addEventListener("click", function() {
    if (tasks.style.fontWeight == "bold") {
        tasks.style.fontWeight = "normal";
    } else {
        tasks.style.fontWeight = "bold"
        dashboard.style.fontWeight = "normal";
        students.style.fontWeight = "normal";
    }
});

select_all_table.addEventListener("click",function (){
    table = this.parentNode.parentNode.parentNode;
    var checkboxes = document.querySelectorAll('#the_table input[type="checkbox"]');
    if(checkboxes[0].checked==false) {
        for (var i = 1; i < checkboxes.length; i++) {
            checkboxes[i].checked = false;
        }
    }
    if(checkboxes[0].checked==true) {
        for (var i = 1; i < checkboxes.length; i++) {
            checkboxes[i].checked = true;
        }
    }
});


function allLetter(inputtxt)
{
    var letters = /^[A-Za-z]+$/;
    if(inputtxt.value.match(letters))
    {
        return true;
    }
    else
    {
        alert("Please write only letters");
        return false;
    }
}

// sup

if('serviceWorker' in navigator){
    window.addEventListener('load',()=>{
        navigator.serviceWorker.register('sw.js').then(registration => {
            console.log('ServiceWorker registred');
            console.log(registration.scope);

        },err=>{
            console.log(err);
            console.log('ServiceWorker failed');

        });
    });
}
function add_server(){
    const data = new FormData(form);
    data.append("type","add")
    for (const [key, value] of data.entries()) {
        console.log(key, value);
    }
    fetch('/action.php', {
        method: 'POST',
        body: data
    })
        .then(response => response.json())
        .then(data => {
            console.log(data);
            var new_student = new Student();
            new_student.id = data.id;
            new_student.fill_student();
            new_student.add_student();
            console.log(new_student);
            Students_List.push(new_student);
        })
        .catch(error => {
            console.error("ERROR IS" + error);
            var rowCount = table_students.rows.length;
            table_students.deleteRow(rowCount -1);
            return false;
        });

    return true;
}
form.addEventListener('submit', function (event){
    event.preventDefault();
});