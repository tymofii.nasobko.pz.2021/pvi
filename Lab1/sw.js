var CACHE_NAME = 'my-site-cache-v1';
var urlsToCache = ['style.css','script.js','index.php','action.php'];

self.addEventListener('install',event=>{
    event.waitUntil(caches.open(CACHE_NAME).then(cache=>{
            return cache.addAll(urlsToCache);
        })
    );
})

self.addEventListener('activate',function (event){
    event.waitUntil(
        caches.keys().then(function (urlsToCache){
            return Promise.all(
                urlsToCache.filter(function (urlsToCache){

                }).map(function (cacheName){
                    return caches.delete(urlsToCache);
                })
            );
        })
    );
});
self.addEventListener('fetch', event => {
    event.respondWith(
        caches.match(event.request)
            .then(function(response){
                    if(response){
                        return response;
                    }
                    return fetch(event.request);
                }
            )
    );
});